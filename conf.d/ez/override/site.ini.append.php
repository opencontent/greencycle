<?php /* #?ini charset="utf-8"?

[ExpiryHandler]
ExpiryFilePerSiteAccess=enabled

[HTTPHeaderSettings]
ClientIpByCustomHTTPHeader=X-Forwarded-For
CustomHeader=enabled
OnlyForAnonymous=enabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=7200

[SearchSettings]
LogSearchStats=disabled

[SiteSettings]
SiteURL=greencycle.localtest.me
AdditionalLoginFormActionURL=http://greencycle.localtest.me/backend/user/login

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=openpa
Password=openp4ssword
Database=greencycle
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[FileSettings]
VarDir=var/greencycle

[RoleSettings]
PolicyOmitList[]=user/login
PolicyOmitList[]=user/logout
PolicyOmitList[]=user/do_logout
PolicyOmitList[]=user/register
PolicyOmitList[]=user/activate
PolicyOmitList[]=user/success
PolicyOmitList[]=user/forgotpassword
PolicyOmitList[]=layout
PolicyOmitList[]=switchlanguage
PolicyOmitList[]=ezjscore/hello
PolicyOmitList[]=ezjscore/call
PolicyOmitList[]=openpa/classdefinition
PolicyOmitList[]=openpa/calendar
PolicyOmitList[]=openpa/object
PolicyOmitList[]=openpa/data
PolicyOmitList[]=robots.txt
PolicyOmitList[]=robots.txt
PolicyOmitList[]=feed/rss
PolicyOmitList[]=feed/list
PolicyOmitList[]=exportas/csv
PolicyOmitList[]=exportas/xml
PolicyOmitList[]=exportas/custom
PolicyOmitList[]=ezinfo/is_alive
PolicyOmitList[]=userpaex/password
PolicyOmitList[]=userpaex/forgotpassword
PolicyOmitList[]=opendata/api
PolicyOmitList[]=gdpr/acceptance
PolicyOmitList[]=gdpr/user_acceptance

[Session]
SessionNameHandler=custom
CookieHttponly=true
Handler=ezpSessionHandlerDB
ForceStart=disabled

[SiteSettings]
MetaDataArray[author]=Trentino Digitale SPA
MetaDataArray[copyright]=GreenCycle
MetaDataArray[description]=Pushing toward Circular Economy in the Alpine Space
MetaDataArray[keywords]=Circular economy, marketplace, recycle, re-use
SiteList[]
SiteList[]=greencycle_backend
SiteList[]=greencycle_debug
SiteList[]=greencycle_frontend
SiteList[]=greencycle_fre_frontend
SiteList[]=greencycle_ger_frontend
SiteList[]=greencycle_ita_frontend
SiteList[]=greencycle_slo_frontend

[UserSettings]
LogoutRedirect=/?logout
MinPasswordLength=8
#GeneratePasswordIfEmpty=false

[SiteAccessSettings]
ForceVirtualHost=true
DebugAccess=enabled
CheckValidity=false
MatchOrder=host_uri
RelatedSiteAccessList[]
RelatedSiteAccessList[]=greencycle_backend
RelatedSiteAccessList[]=greencycle_debug
RelatedSiteAccessList[]=greencycle_frontend
RelatedSiteAccessList[]=greencycle_fre_frontend
RelatedSiteAccessList[]=greencycle_ger_frontend
RelatedSiteAccessList[]=greencycle_ita_frontend
RelatedSiteAccessList[]=greencycle_slo_frontend
AvailableSiteAccessList[]
AvailableSiteAccessList[]=greencycle_backend
AvailableSiteAccessList[]=greencycle_debug
AvailableSiteAccessList[]=greencycle_frontend
AvailableSiteAccessList[]=greencycle_fre_frontend
AvailableSiteAccessList[]=greencycle_ger_frontend
AvailableSiteAccessList[]=greencycle_ita_frontend
AvailableSiteAccessList[]=greencycle_slo_frontend
HostUriMatchMapItems[]
HostUriMatchMapItems[]=marketplace.greencycle.si;backend;greencycle_backend
HostUriMatchMapItems[]=marketplace.greencycle.si;debug;greencycle_debug
HostUriMatchMapItems[]=marketplace.greencycle.si;fr;greencycle_fre_frontend
HostUriMatchMapItems[]=marketplace.greencycle.si;de;greencycle_ger_frontend
HostUriMatchMapItems[]=marketplace.greencycle.si;it;greencycle_ita_frontend
HostUriMatchMapItems[]=marketplace.greencycle.si;sl;greencycle_slo_frontend
HostUriMatchMapItems[]=marketplace.greencycle.si;;greencycle_frontend
HostUriMatchMapItems[]=greencycle.localtest.me;backend;greencycle_backend
HostUriMatchMapItems[]=greencycle.localtest.me;debug;greencycle_debug
HostUriMatchMapItems[]=greencycle.localtest.me;fr;greencycle_fre_frontend
HostUriMatchMapItems[]=greencycle.localtest.me;de;greencycle_ger_frontend
HostUriMatchMapItems[]=greencycle.localtest.me;it;greencycle_ita_frontend
HostUriMatchMapItems[]=greencycle.localtest.me;sl;greencycle_slo_frontend
HostUriMatchMapItems[]=greencycle.localtest.me;;greencycle_frontend

[MailSettings]
Transport=sendmail

[EmbedViewModeSettings]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]=embed-inline

[TimeZoneSettings]
TimeZone=Europe/Rome

[RegionalSettings]
Locale=ita-IT
TextTranslation=enabled

[ContentSettings]
ContentObjectNameLimit=203
TranslationList=

[DebugSettings]
DebugToolbar=disabled

[UserFormToken]
CookieHttponly=true
CookieSecure=0

 
*/ ?>
