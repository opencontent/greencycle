<?php /* #?ini charset="utf-8"?

[SiteSettings]
SiteName=GreenCycle
SiteURL=marketplace.greencycle.si/backend
DefaultPage=content/dashboard
LoginPage=custom

[SiteAccessSettings]
RequireUserLogin=true
ShowHiddenNodes=true
RelatedSiteAccessList[]=greencycle_backend
RelatedSiteAccessList[]=greencycle_debug
RelatedSiteAccessList[]=greencycle_frontend
RelatedSiteAccessList[]=greencycle_fre_frontend
RelatedSiteAccessList[]=greencycle_ger_frontend
RelatedSiteAccessList[]=greencycle_ita_frontend
RelatedSiteAccessList[]=greencycle_slo_frontend

[DesignSettings]
SiteDesign=backend
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=eng-GB
ContentObjectLocale=eng-GB
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-GB
SiteLanguageList[]=ita-IT
SiteLanguageList[]=fre-FR
SiteLanguageList[]=ger-DE
SiteLanguageList[]=slo-SI
TextTranslation=enabled

[FileSettings]
VarDir=var/greencycle

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[UserSettings]
RegistrationEmail=

[InformationCollectionSettings]
EmailReceiver=

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ocmediaplayer
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=ocrss
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=openpa_theme_2014
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocopendata_forms
ActiveAccessExtensions[]=ocopendata_forms_design
ActiveAccessExtensions[]=openpa_designitalia
ActiveAccessExtensions[]=greencycle
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=nglanguageswitcher
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=occhart


[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;user/register
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
*/ ?>
