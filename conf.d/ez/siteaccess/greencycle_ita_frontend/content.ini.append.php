<?php /* #?ini charset="utf-8"?

[ClassAttributeSettings]
CategoryList[content]=Contenuti principali
CategoryList[meta]=Meta
CategoryList[hidden]=Nascosto
CategoryList[details]=Contenuti secondari
CategoryList[taxonomy]=Categorizzazione

[VersionManagement]
DefaultVersionHistoryLimit=10
VersionHistoryClass[]

[VersionView]
AvailableSiteDesignList[]
AvailableSiteDesignList[]=ezflow_site_clean
AvailableSiteDesignList[]=admin2
AvailableSiteDesignList[]=admin
*/ ?>