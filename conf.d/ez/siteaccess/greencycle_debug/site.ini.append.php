<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ocmediaplayer
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=ocrss
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=openpa_theme_2014
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocopendata_forms
ActiveAccessExtensions[]=ocopendata_forms_design
ActiveAccessExtensions[]=openpa_designitalia
ActiveAccessExtensions[]=greencycle
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=nglanguageswitcher
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary


[Session]
SessionNamePerSiteAccess=enabled

[SiteSettings]
SiteName=GreenCycle
SiteURL=marketplace.greencycle.si
LoginPage=embedded
AdditionalLoginFormActionURL=http://marketplace.greencycle.si/backend/user/login
MetaDataArray[author]=Trentino Digitale SPA
MetaDataArray[copyright]=GreenCycle
MetaDataArray[description]=Pushing toward Circular Economy in the Alpine Space
MetaDataArray[keywords]=Circular economy, marketplace, recycle, re-use

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
RelatedSiteAccessList[]=greencycle_backend
RelatedSiteAccessList[]=greencycle_debug
RelatedSiteAccessList[]=greencycle_frontend

[DesignSettings]
SiteDesign=greencycle
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=designitalia
AdditionalSiteDesignList[]=openpa_solid
AdditionalSiteDesignList[]=ocbootstrap
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=eng-GB
ContentObjectLocale=eng-GB
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-GB
SiteLanguageList[]=ita-IT
SiteLanguageList[]=fre-FR
SiteLanguageList[]=ger-DE
SiteLanguageList[]=slo-SI
TextTranslation=enabled
TranslationSA[greencycle_frontend]=English
TranslationSA[greencycle_fre_frontend]=Française
TranslationSA[greencycle_ger_frontend]=Deutsch 
TranslationSA[greencycle_ita_frontend]=Italiano
TranslationSA[greencycle_slo_frontend]=Slovenski
LanguageSA[eng-GB]=greencycle_frontend
LanguageSA[fre-FR]=greencycle_fre_frontend
LanguageSA[ger-DE]=greencycle_ger_frontend
LanguageSA[ita-IT]=greencycle_ita_frontend
LanguageSA[slo-SI]=greencycle_slo_frontend

[FileSettings]
VarDir=var/greencycle

[MailSettings]
AdminEmail=webmaster@opencontent.it
EmailSender=

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
RegistrationEmail=

[ContentSettings]
TranslationList=
#################
##### DEBUG #####
#################
 
[MailSettings]
Transport=file

[ContentSettings]
ViewCaching=disabled

[DebugSettings]
DebugOutput=enabled
DebugRedirection=disabled
AlwaysLog[]=warning
AlwaysLog[]=debug
AlwaysLog[]=notice
AlwaysLog[]=strict

[TemplateSettings]
DevelopmentMode=enabled
Debug=enabled
ShowXHTMLCode=disabled
TemplateCache=disabled
TemplateCompile=disabled
ShowUsedTemplates=enabled

[OverrideSettings]
Cache=disabled

[DesignSettings]
DesignLocationCache=disabled

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;user/register
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
*/ ?>
