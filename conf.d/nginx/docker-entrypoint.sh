#!/bin/bash
set -ex

if [[ -z PROMETHEUS_DISABLE ]]; then
  echo "==> Starting prometheus metrics exporter"
  /usr/bin/exporter -nginx.scrape-uri http://localhost:80/nginx_status &
fi

echo "==> Starting nginx... good luck!"
exec nginx -g 'daemon off;'

